<?php
function ubah_huruf($string){
//kode di sini
$a = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
$b = "";
foreach (str_split($string) as $c) {
  for ($i=0; $i < count($a); $i++) {
    if ($c == $a[$i]) {
      $b .= $a[$i+1];
    }
  }
}
return "$b<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>
